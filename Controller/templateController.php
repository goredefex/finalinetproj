<?php 
	
	class TemplateController {
		
		
		public function buildTemplateObjects() {
			
			//Find Data To Send To Views
			include_once "../Model/templateMapping.php";
			$tempMap = new TemplateMapping();
			$tempsRecieved = $tempMap->getAllTemplates();
			
			//Send Data Found To Views
			include_once "../View/frontEndViews.php";
			$newView = new PageViews();
			$mainPage = $newView->viewTempPage($tempsRecieved);
								
			echo $mainPage;
			
		} //end function
		
		public function deleteTemplate($tempId) {
			
			//Find Data To Send To Views
			include_once "../Model/templateMapping.php";
			$tempMap = new TemplateMapping();
			$tempsRecieved = $tempMap->deleteTemplate($tempId);
			
			header("Location: ../index.php"); //header("Location: ".$_SERVER['HTTP_HOST']);
			exit;
			
		} //end function
		
		public function addTemplate($backingChoice, $textChoice, $newDesc) {
			
			//Find Data To Send To Views
			include_once "../Model/templateMapping.php";
			$tempMap = new TemplateMapping();
			$tempsRecieved = $tempMap->addTemplate($backingChoice, $textChoice, $newDesc);
			
			header("Location: ../index.php"); //header("Location: ".$_SERVER['HTTP_HOST']);
			exit;
			
		} //end function
		
		
		
	} //end class

?>