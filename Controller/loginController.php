<?php 

	class LoginController {
		
		public function processLogin($currUser, $currPass) {
			
			//Find Data To Process
			include_once "../Model/LoginMapping.php";
			$loginMap = new LoginMapping();
			$loginRecieved = $loginMap->processAndMapLogin($currUser, $currPass);
			
		} //end function
		
		
		public function processLogout() {
			
			//Find Data To Process
			include_once "../Model/LoginMapping.php";
			$loginMap = new LoginMapping();
			$loginRecieved = $loginMap->logout();
			
		} //end function
		
		
		public function registerUser($currUser, $currPass, $fName, $lName) {			
						
			//Send Data To Process
			include_once "../Model/LoginMapping.php";
			$loginMap = new LoginMapping();
			$loginRecieved = $loginMap->registerUserMapping($currUser, $currPass, $fName, $lName);
			
		} //end function
		
		
	}

?>