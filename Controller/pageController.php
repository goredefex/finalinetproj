<?php 

	//CRUD Controller
	class PageController {
		
				
		//Read
		public function buildLandPage() {
			
			//Find Data To Send To Views
			include_once "../Model/pageMapping.php";
			include_once "../Model/placementMapping.php";
			include_once "../Model/articleMapping.php";
			include_once "../Model/templateMapping.php";
			
			$pageMap = new PageMapping();
			$pageRecieved = $pageMap->buildAPage(1);
			$placementMap = new PlacementMapping();
			$placementsRecieved = $placementMap->getPlacements(1);
			$tempMap = new TemplateMapping();
			$templateRecieved = $tempMap->getTemplate($pageRecieved->getTempId());
			$articlesRecieved;
			
			for($i=0; $i<count($placementsRecieved); $i++) {
				$articleMap = new ArticleMapping();
				$articlesRecieved[] = $articleMap->buildArticleObject($placementsRecieved[$i]->getArticleId());	
				
			}												
										
			//Send Data Found To Views
			include_once "../View/frontEndViews.php";
			$newView = new PageViews();
			$mainPage = $newView->viewAPage($pageRecieved, $placementsRecieved, $articlesRecieved, $templateRecieved, NULL);
					
			
			echo $mainPage;
			
		} //end function
				
		
		public function buildDebatesPage() {
			
			//Find Data To Send To Views
			include_once "../Model/pageMapping.php";
			include_once "../Model/placementMapping.php";
			include_once "../Model/articleMapping.php";
			include_once "../Model/templateMapping.php";
			
			$pageMap = new PageMapping();
			$pageRecieved = $pageMap->buildAPage(2);
			$placementMap = new PlacementMapping();
			$placementsRecieved = $placementMap->getPlacements(2);
			$tempMap = new TemplateMapping();
			$templateRecieved = $tempMap->getTemplate($pageRecieved->getTempId());
			$articlesRecieved;
			
			for($i=0; $i<count($placementsRecieved); $i++) {
				$articleMap = new ArticleMapping();
				$articlesRecieved[] = $articleMap->buildArticleObject($placementsRecieved[$i]->getArticleId());	
				
			}												
										
			//Send Data Found To Views
			include_once "../View/frontEndViews.php";
			$newView = new PageViews();
			$mainPage = $newView->viewAPage($pageRecieved, $placementsRecieved, $articlesRecieved, $templateRecieved);
					
			
			echo $mainPage;
						
		} //end function
		
		
		
		public function generateDebatePage($id) {
			
			//Find Data To Send To Views
			include_once "../Model/pageMapping.php";
			$pageMap = new PageMapping();
			$pageRecieved = $pageMap->getPageObjects($id);
			
			//Send Data Found To Views
			include_once "../View/frontEndViews.php";
			$newView = new PageViews();
			$mainPage = $newView->viewAPage($pageRecieved, NULL);
						
			echo $mainPage;
			
		} //end function
		
		
		
	} //end class

?>