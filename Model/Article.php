<?php 

	class Article {
		
		
		//Member Vars --------------------------------------------- 
		private $id;
		private $content;
		private $active;
		private $dateMade;
		private $createdBy;
		private $lastModifiedBy;
		private $title;
		
		
		
		//Constructs ---------------------------------------------
		public function __construct() {
			
			$this->setId(0);
			$this->setContent("None");
			$this->setActive(false);
			$this->setDateMade("NULL");
			$this->setLastModified(0);
			$this->setTitle("None");
			$this->setCreatedBy(0);
						
		}
		
		
		
		//Setters ------------------------------------------------	
		public function setDateMade($newDate) {
			$this->dateMade = $newDate;
			
		} //end function
		
		public function setId($newId) {
			$this->id = $newId;
			
		} //end function
		
		public function setContent($newContent) {
			$this->content = $newContent;
			
		} //end function
		
		public function setActive($newActiveState) {
			$this->active = $newActiveState;
			
		} //end function
		
		public function setLastModified($newUser) {
			$this->lastModifiedBy = $newUser;
			
		} //end function
		
		public function setTitle($newTitle) {
			$this->title = $newTitle;
			
		} //end function
						
		public function setCreatedBy($newPerson) {
			$this->createdBy = $newPerson;
			
		} //end function				
		
		
		
		//Getters ------------------------------------------------
		public function getDateMade() {
			return $this->dateMade;
			
		} //end function
		
		public function getId() {
			return $this->id;
			
		} //end function		
		
		public function getContent() {
			return $this->content;
			
		} //end function
		
		public function getActiveState() {
			return $this->active;
			
		} //end function
		
		public function getLastModified() {
			return $this->lastModifiedBy;
			
		} //end function
		
		public function getTitle() {
			return $this->title;
			
		} //end function
		
		public function getCreatedBy() {
			return $this->createdBy;
			
		} //end function
		
		
	} //end class
	
?>