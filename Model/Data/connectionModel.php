<?php 

	class Connect {
	
		private $errorMsg = "sorry the site is currently having technical problems";
		private $test = "it was the dbas name";
		private $link;
	
		public function connectTo() {
			
			if(isset($_SESSION['authority']) && strcmp($_SESSION['authority'], "admin")) {
				$this->link = mysqli_connect('localhost', 'root', '') or die($this->errorMsg);			
				mysqli_select_db($this->link, 'cms') or die(mysql_error()." ".$this->test);
									
			} else {
				$this->link = mysqli_connect('localhost', 'regular', '') or die($this->errorMsg);			
				mysqli_select_db($this->link, 'cms') or die(mysql_error()." ".$this->test);
			
			}
														
			return $this->link;
			
		}
		
		public function closeConn() {
			mysqli_close($this->link);
			
		} //end function
				
	
	} //end class
?>