<?php 
		
	class Query {
						
		//Member Vars
		private $queryString;
		private $link;
		private $conn;
		private $start;
		
		
		//Constructs ---------------------------------------------
		
		public function __construct() {
			
			include_once "connectionModel.php";
			$this->conn = new Connect();
			$this->link = $this->conn->connectTo();
						
			
		} //end constructor
		
		public function __destruct() {
			
			$this->conn->closeConn();
			
		} //end destructor 
		
		
		
		//Getters ---------------------------------------------
		
		public function getQueryString() {
			return $this->queryString;			
			
		} //end function
		
		public function getLink() {
			return $this->link;
			
		} //end function
		
		
		//Logins -----------------------------------------------------------------------
		public function getUserString($currUser) {
			$currUser = mysqli_real_escape_string($this->link, $currUser);
			return "SELECT * 
					FROM `users` u
					INNER JOIN authorities auth
					ON u.auth_id = auth.auth_id
					WHERE username = '".$currUser."'";
			
		} //end function
		
		public function getRegisterUser($newUser, $newPass, $fName, $lName) {
			$hashedPass = hash("sha256", "ytv6ytv66".$newPass."y5fkvo1fu");
			$cleansedPass;
			
			for($i=0; $i<3000; $i++) {
				$cleansedPass = hash("sha256", $hashedPass);
				
			}
				
			return "INSERT INTO `users` (
					`user_id` ,
					`username` ,
					`password` ,
					`auth_id` ,
					`firstname` ,
					`lastname` ,
					`date_joined`
					)
					VALUES (NULL , '".$newUser."', '".$cleansedPass."', '4', '".$fName."', '".$lName."', CURRENT_DATE())";	
			
		} //end function
		
		//Templates -------------------------------------------------------------------
		public function queryAllTemplates() {
			return "SELECT * 
					FROM  `templates` 
					WHERE 1";
			
		} //end function
		
		public function queryTemplateDeletion($tempId) {
			return "DELETE FROM `templates` WHERE `templates`.`template_id` = ".$tempId;
			
		} //end function
		
		public function queryAddTemplate($backingChoice, $textChoice, $newDesc) {
			return "INSERT INTO `templates` (`template_id` ,
											 `template_backColor` ,
											 `template_textColor` ,
											 `template_description`
											)
					VALUES (NULL, '".$backingChoice."', '".$textChoice."', '".$newDesc."')";
			
		} //end function
		
		
		
		// Pages ----------------------------------------------------------------------
		public function queryAPage($pageId) {
			return "SELECT * 
					FROM  `pages`
					WHERE page_id = ".$pageId;
								
		} //end function
		
		public function queryPagePlacements($pageId) {
			return "SELECT * FROM `article_page_placement` WHERE page_id = ".$pageId;
								
		} //end function
		
		public function queryArticles($articleId) {
			return "SELECT * 
					FROM  `articles`
					WHERE article_id = ".$articleId;
								
		} //end function
		
		public function queryTemplates($tempId) {
			return "SELECT * 
					FROM `templates` 
					WHERE template_id = ".$tempId;
			
		} //end function
		
		
		//Posts -----------------------------------------------------------------------
		public function queryAllPostPages() {
			return "SELECT * 
					FROM  `pages` p
					INNER JOIN users u ON p.page_created_by = u.user_id
					WHERE p.page_id > 3";
			
		} //end function
		
		
		// ##########################################################################
		//Construction Zone ---------------------------------------------------------
		
		
		
		public function queryAdminPages($choice) {
			return "SELECT * 
					FROM  `article_page_placement` app
					INNER JOIN pages p ON app.page_id = p.page_id
					INNER JOIN articles art ON app.article_id = art.article_id
					INNER JOIN users u ON p.page_created_by = u.user_id
					INNER JOIN authorities auth ON u.auth_id = auth.auth_id
					INNER JOIN templates t ON p.template_id = t.template_id
					WHERE auth.auth_name = 'Admin' AND p.page_id = ".$choice;
			
		} //end function
		
		public function queryByPlacement($placementId) {
			return "SELECT * 
					FROM  `pages` p
					INNER JOIN article_page_placement app ON p.page_id = app.page_id
					INNER JOIN articles art ON app.article_id = art.article_id
					INNER JOIN templates t ON p.template_id = t.template_id
					WHERE app.placement_id = ".$placementId;
			
		} //end function
		
		public function queryPageByPlacement($page) {
			return "SELECT * 
					FROM  `pages` p
					INNER JOIN article_page_placement app ON p.page_id = app.page_id
					INNER JOIN articles art ON app.article_id = art.article_id
					INNER JOIN templates t ON p.template_id = t.template_id
					WHERE app.placement_id = ".$page;
			
		} //end function
				
		
		public function queryAllPosts($id) {
			return "SELECT * 
					FROM  `pages` p
					INNER JOIN article_page_placement app ON p.page_id = app.page_id
					INNER JOIN articles art ON app.article_id = art.article_id
					INNER JOIN templates t ON p.template_id = t.template_id
					INNER JOIN users u ON p.page_created_by = u.user_id
					WHERE p.page_id = ".$id;				
			
		} //end function
		
		public function search($searchCriteria) {
			return "SELECT * 
					FROM  `pages` p
					INNER JOIN article_page_placement app ON p.page_id = app.page_id
					INNER JOIN articles art ON app.article_id = art.article_id
					INNER JOIN templates t ON p.template_id = t.template_id
					INNER JOIN users u ON p.page_created_by = u.user_id
					WHERE p.page_name LIKE '%".$searchCriteria."%'";	
			
		} //end function
		
		
		
		
		//Setters ---------------------------------------------
		
		public function setQueryString($newString) {
			$this->queryString = $newString;
			
		} //end function
		
		
		
		
		//Functions ---------------------------------------------
		
		public function queryDatabase() {
			return mysqli_query($this->link, $this->getQueryString());
			
		} //end function
		
				
		
		
		
	} //end class


?>