<?php 

	class PlacementMapping {
		
		
		private $placementArray;
		
		
		//Functions --------------------------------------------------
		
		public function getPlacements($pageId) {
			
			//Make Query Modeling
			include_once "Data/queryModel.php";	
			$query = new Query();
			$query->setQueryString($query->queryPagePlacements($pageId));
			$results = $query->queryDatabase();
			
			//Include Customer Entity
			include_once "Placement.php";
			
			while($row = mysqli_fetch_assoc($results)) {
			
				$currPlace = new Placement();
				$currPlace->setId($row['page_id']);
				$currPlace->setPageId($pageId);
				$currPlace->setArticleId($row['article_id']);
				$currPlace->setLocation($row['where_location']);
				$this->placementArray[] = $currPlace;
			
			}
			
			return $this->placementArray;
			
		} //end function
		
		
		
	} //end function

?>