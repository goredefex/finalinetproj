<?php 

	class Template {
		
		//Member Vars --------------------------------------------- 
		private $templateId;
		private $backColor;
		private $textColor;
		private $description;
		
		
		//Setters ------------------------------------------------		
		public function setTempId($newId) {
			$this->templateId = $newId;
			
		} //end function
		
		public function setBackColor($newColor) {
			$this->backColor = $newColor;
			
		} //end function
		
		public function setTextColor($newColor) {
			$this->textColor = $newColor;
			
		} //end function
		
		public function setDescription($newDesc) {
			$this->description = $newDesc;
			
		} //end function
		
		
		
		//Getters ------------------------------------------------
		public function getTempId() {
			return $this->templateId;
			
		} //end function
		
		public function getBackColor() {
			return $this->backColor;
			
		} //end function
		
		public function getTextColor() {
			return $this->textColor;
			
		} //end function
		
		public function getDescription() {
			return $this->description;
			
		} //end function
		
		
		
	} //end class

?>