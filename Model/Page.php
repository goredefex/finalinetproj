<?php 

	class Page {
		
		
		//Member Vars --------------------------------------------- 
		private $id;
		private $templateId;
		private $pageContent;
		private $pageTitle;
		private $dateMade;
		private $lastModifiedBy;
		private $createdBy;
		private $pageUrl;
		
		
		
		//Constructs ---------------------------------------------
		public function __construct() {
			
			$this->setId(0);
			$this->setTempId(0);
			$this->setPageContent("None");
			$this->setPageName("None");
			$this->setDateMade("NULL");
			$this->setLastModified(0);
			$this->setUrl("None");
						
		}
		
		
		//Setters ------------------------------------------------		
		public function setId($newId) {
			$this->id = $newId;
			
		} //end function
		
		public function setLastModified($newUser) {
			$this->lastModifiedBy = $newUser;
			
		} //end function
		
		public function setTempId($newId) {
			$this->templateId = $newId;
			
		} //end function
		
		public function setPageContent($newContent) {
			$this->pageContent = $newContent;
			
		} //end function
						
		public function setDateMade($newDate) {
			$this->dateMade = $newDate;
			
		} //end function
		
		public function setPageName($newTitle) {
			$this->pageTitle = $newTitle;
			
		} //end function
		
		public function setCreatedBy($newPerson) {
			$this->createdBy = $newPerson;
			
		} //end function	
		
		public function setUrl($newUrl) {
			$this->pageUrl = $newUrl;
			
		} //end function
						
		
		
		
		//Getters ------------------------------------------------
		public function getId() {
			return $this->id;
			
		} //end function
		
		public function getLastModified() {
			return $this->lastModifiedBy;
			
		} //end function
		
		public function getTempId() {
			return $this->templateId;
			
		} //end function
		
		public function getPageContent() {
			return $this->pageContent;
			
		} //end function
				
		public function getPageName() {
			return $this->pageTitle;
			
		} //end function
				
		public function getDateMade() {
			return $this->dateMade;
			
		} //end function		
		
		public function getCreatedBy() {
			return $this->createdBy;
			
		} //end function			
		
		public function getUrl() {
			return $this->pageUrl;			
			
		} //end function
		
		
		
		
	} //end class

?>