<?php 

	class ArticleMapping {
			
		
		private $articleArray;
		
		
		public function buildArticleObject($articleId) {
			
			//Make Query Modeling
			include_once "Data/queryModel.php";	
			$query = new Query();
			$query->setQueryString($query->queryArticles($articleId));
			$results = $query->queryDatabase();
			
			//Include Customer Entity
			include_once "Article.php";
						
			$row = mysqli_fetch_assoc($results);
				
			$currArticle = new Article();
			$currArticle->setId($row['article_id']);
			$currArticle->setContent($row['article_content']);
			$currArticle->setActive($row['article_active']);
			$currArticle->setTitle($row['title']);
			$currArticle->setCreatedBy($row['article_created_by']);
			$currArticle->setDateMade($row['article_post_date']);
			$currArticle->setLastModified($row['last_modified_by']);
			$this->articleArray[] = $currArticle;
			
			return $currArticle;
			
		} //end function
		
		
	} //end function

?>