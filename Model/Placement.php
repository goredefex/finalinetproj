<?php 

	class Placement {
		
		
		//Member Vars --------------------------------------------- 
		private $id;
		private $pageId;
		private $articleId;
		private $location;
		
		
		
		//Constructs ---------------------------------------------
		public function __construct() {
			
			$this->setId(0);
			$this->setPageId(0);
			$this->setArticleId(0);
			$this->setLocation(0);
						
		}
		
		
		
		//Setters ------------------------------------------------	
		public function setId($newId) {
			$this->id = $newId;
			
		} //end function
		
		public function setPageId($newId) {
			$this->pageId = $newId;
			
		} //end function
		
		public function setArticleId($newId) {
			$this->articleId = $newId;
			
		} //end function
		
		public function setLocation($newLocale) {
			$this->location = $newLocale;
			
		} //end function
		
		
		
		//Getters ------------------------------------------------
		public function getId() {
			return $this->id;
			
		} //end function	
		
		public function getPageId() {
			return $this->pageId;
			
		} //end function	
		
		public function getArticleId() {
			return $this->articleId;
			
		} //end function	
		
		public function getLocation() {
			$this->location;
			
		} //end function
		
		
		
		
	} //end class

?>