<?php 

	class TemplateMapping {
		
		private $tempArray;
		
		public function getAllTemplates() {			
			
			//Make Query Modeling
			include_once "Data/queryModel.php";	
			$query = new Query();
			$query->setQueryString($query->queryAllTemplates());
			$results = $query->queryDatabase();
			
			//Include Customer Entity
			include_once "Template.php";
						
			while($row = mysqli_fetch_assoc($results)) {
			
				$currTemp = new Template();
				$currTemp->setTempId($row['template_id']);
				$currTemp->setBackColor($row['template_backColor']);
				$currTemp->setTextColor($row['template_textColor']);
				$currTemp->setDescription($row['template_description']);
				$this->tempArray[] = $currTemp;
			
			}	
			
			return $this->tempArray;
			
		} //end function
				
		
		public function getTemplate($tempId) {			
			
			//Make Query Modeling
			include_once "Data/queryModel.php";	
			$query = new Query();
			$query->setQueryString($query->queryTemplates($tempId));
			$results = $query->queryDatabase();
			
			//Include Customer Entity
			include_once "Template.php";
						
			$row = mysqli_fetch_assoc($results);
			
			$currTemp = new Template();
			$currTemp->setTempId($tempId);
			$currTemp->setBackColor($row['template_backColor']);
			$currTemp->setTextColor($row['template_textColor']);	
			
			return $currTemp;		
			
		} //end function
		
		public function deleteTemplate($tempId) {
			
			//Make Query Modeling
			include_once "Data/queryModel.php";	
			$query = new Query();
			$query->setQueryString($query->queryTemplateDeletion($tempId));
			$results = $query->queryDatabase();
			
			if($results) {
				$_SESSION['statusUpdate'] = "Successfully Deleted!";
				
			}
			
		} //end function
		
		public function addTemplate($backingChoice, $textChoice, $newDesc) {
			
			//Make Query Modeling
			include_once "Data/queryModel.php";	
			$query = new Query();
			$query->setQueryString($query->queryAddTemplate($backingChoice, $textChoice, $newDesc));
			$results = $query->queryDatabase();
			
			if($results) {
				$_SESSION['statusUpdate'] = "Successfully Added!";
				
			}
			
		} //end function
		
		
		
	} //end class

?>