<?php 

	class LoginMapping {
		
		
		//Correction And Extensive Re-Assertion Algorithm
		public function processAndMapLogin($currUser, $currPass) {
			
			if ($currUser) {
				if ($currPass) {
					
					//Make Query Modeling
					include_once "Data/queryModel.php";	
					$query = new Query();
					$query->setQueryString($query->getUserString($currUser));
					$results = $query->queryDatabase();
					
					$userFound = mysqli_num_rows($results);
					
					if ($userFound >= 1) {
					
						//Grab the password hash
						$row = mysqli_fetch_assoc($results);
						$dbid = $row['user_id'];
						$dbuser = $row['username']; 
						$dbpass = $row['password'];
						$_SESION["dbAuth"] = $row['auth_name'];
						$dbAuth = $row['auth_name'];
						
						//Rehashing
						$hashedPass = hash("sha256", "ytv6ytv66".$currPass."y5fkvo1fu");
						$cleansedPass;
						
						for($i=0; $i<3000; $i++) {
							$cleansedPass = hash("sha256", $hashedPass);
							
						}
						
						//Check to see if the user entered their password correctly.
						if ($cleansedPass === $dbpass) {
							
								$_SESSION['userid'] = $dbid;
								$_SESSION['username'] = $dbuser;
								$_SESSION['nowLoggedIn'] = true;
								$_SESSION['authority'] = $dbAuth;								
								
								$_SESSION['statusUpdate'] = "Welcome, ".$dbuser."  -  User Num: ".$dbid." - Authority Level: '".$dbAuth."'";
								
								header("Location: ../index.php"); //header("Location: ".$_SERVER['HTTP_HOST']);
								exit;
		
						} else {
							
							
							$_SESSION['statusUpdate'] = "Invalid password";
							
							header("Location: ../index.php"); //header("Location: ".$_SERVER['HTTP_HOST']);
							exit;
		
						} //end password check
						
					//If the database returned 0 rows for the login.
					} else {
						
						$_SESSION['statusUpdate'] = "The Username you have entered was not found";
						
						header("Location: ../index.php"); //header("Location: ".$_SERVER['HTTP_HOST']);
						exit;
						
				    } //end row check
		
				// If the user didn't enter a password.
				} else {
					
					$_SESSION['statusUpdate'] = "You must enter a password";
					
					header("Location: ../index.php"); //header("Location: ".$_SERVER['HTTP_HOST']);
					exit;
					
				}
				
			// If the user didnt enter a username.
			} else {
				
				$_SESSION['statusUpdate'] = "You must enter your username";
				
				header("Location: ../index.php"); //header("Location: ".$_SERVER['HTTP_HOST']);
				exit;
				
			}
			
			
		} //end function
		
		
		public function logout() {
			session_start();
			session_destroy();
			
			$_SESSION['statusUpdate'] = "Successfully Logged Out!";
							
			header("Location: ../index.php"); //header("Location: ".$_SERVER['HTTP_HOST']);
			exit;
			
		} //end function
		
		
		
		
		
		//Process of Hashing, Salting and entering a new non author-user
		public function registerUserMapping($currUser, $currPass, $fName, $lName) {
			
			//Make Query Modeling
			include_once "Data/queryModel.php";	
			$query = new Query();
			$query->setQueryString($query->getRegisterUser($currUser, $currPass, $fName, $lName));
			$results = $query->queryDatabase();
			
			$_SESSION['statusUpdate'] = "Successfully Registered! Try Logging In";
							
			header("Location: ../index.php"); //header("Location: ".$_SERVER['HTTP_HOST']);
			exit;
			
		} //end function
		
		
		
	} //end class

?>