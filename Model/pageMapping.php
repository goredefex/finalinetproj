<?php 

	class PageMapping {
		
		
		private $pageArray;
		private $articleArray;
		
		
		public function buildAPage($pageId) {
			
			//Make Query Modeling
			include_once "Data/queryModel.php";	
			$query = new Query();
			$query->setQueryString($query->queryAPage($pageId));
			$results = $query->queryDatabase();
			
			//Include Customer Entity
			include_once "Page.php";
			$row = mysqli_fetch_assoc($results);
			
			$currPage = new Page();
			$currPage->setId($row['page_id']);
			$currPage->setPageContent($row['page_content']);
			$currPage->setTempId($row['template_id']);
			$currPage->setPageName($row['page_name']);
			$currPage->setCreatedBy($row['page_created_by']);
			$currPage->setDateMade($row['date_created']);
			$currPage->setUrl($row['page_url']);
			$currPage->setLastModified($row['last_edited_by']);
			
			return $currPage;
			
		} //end function
		
		
		
		
		// ##########################################################################
		//Construction Zone ---------------------------------------------------------
		
		public function getAllPageObjects() {
			
			//Make Query Modeling
			include_once "Data/queryModel.php";	
			$query = new Query();
			$query->setQueryString($query->queryAllPages());
			$results = $query->queryDatabase();
			
			//Include Customer Entity
			include_once "Page.php";	
			
			//Loop Data And Return Customer Array
			while($row = mysqli_fetch_assoc($results)) {
				
				$currPage = new Page();
				$currPage->setId($row['page_id']);
				$currPage->setPageContent($row['page_content']);
				$currPage->setTempId($row['template_id']);
				$currPage->addToArticleArray($row[''], $articleId);
				
			}
			
			return $this->pageArray;		
			
		} //end function
		
		
		public function getLandingPageObject() {
			
			//Make Query Modeling
			include_once "Data/queryModel.php";	
			$query = new Query();
			$query->setQueryString($query->queryAdminPages("1"));
			$results = $query->queryDatabase();
			
			//Include Customer Entity
			include_once "Page.php";	
			
			$count = mysqli_num_rows($results);
			
			while($row = mysqli_fetch_assoc($results)) {
				$currPage = new Page();
				$currPage->setId($row['page_id']);
				$currPage->setPageContent($row['page_content']);
				$currPage->setTempId($row['template_id']);
				$currPage->setBackColor($row['template_backColor']);
				$currPage->setTextColor($row['template_textColor']);
				$currPage->addToArticleArray($row['article_content'], $row['where_location']);
				
				if($count>0) {
					while($rowTemp = mysqli_fetch_assoc($results)) {
						$currPage->addToArticleArray($rowTemp['article_content'], $rowTemp['where_location']);
						
					}
				}
				
				$this->pageArray[] = $currPage;
			}
			
			return $this->pageArray;
			
		} //end function
		
		
		public function getDebatesPageObject() {
			
			//Make Query Modeling
			include_once "Data/queryModel.php";	
			$query = new Query();
			$query->setQueryString($query->queryAdminPages("2"));
			$results = $query->queryDatabase();
			
			//Include Customer Entity
			include_once "Page.php";	
			
			$count = mysqli_num_rows($results);
			
			while($row = mysqli_fetch_assoc($results)) {
				$currPage = new Page();
				$currPage->setId($row['page_id']);
				$currPage->setPageContent($row['page_content']);
				$currPage->setTempId($row['template_id']);
				$currPage->setBackColor($row['template_backColor']);
				$currPage->setTextColor($row['template_textColor']);
				$currPage->addToArticleArray($row['article_content'], $row['where_location']);
				
				if($count>0) {
					while($rowTemp = mysqli_fetch_assoc($results)) {
						$currPage->addToArticleArray($rowTemp['article_content'], $rowTemp['where_location']);
						
					}
				}
				
				$this->pageArray[] = $currPage;
			}
			
			return $this->pageArray;
			
		} //end function
		
		
		
		public function getDebatePostViewerObject() {
			
			//Make Query Modeling
			include_once "Data/queryModel.php";	
			$query = new Query();
			$query->setQueryString($query->queryAllPostPages());
			$results = $query->queryDatabase();
			
			//Include Customer Entity
			include_once "Page.php";	
			
			
			while($row = mysqli_fetch_assoc($results)) {
				$currPage = new Page();
				$currPage->setId($row['page_id']);
				$currPage->setPageContent($row['page_content']);
				$currPage->setTempId($row['template_id']);
				$currPage->setBackColor($row['template_backColor']);
				$currPage->setTextColor($row['template_textColor']);
				$currPage->setPageName($row['page_name']);
				$currPage->setWhoMadePage($row['firstname'], $row['lastname']);
				$currPage->addToArticleArray($row['article_content'], $row['where_location']);
				
				$query->setQueryString($query->queryAllPosts($row['page_id']));
				$resultPosts = $query->queryDatabase();
				$count = mysqli_num_rows($resultPosts);
						
				if($count>0) {
					while($rowTemp = mysqli_fetch_assoc($resultPosts)) {
						$currPage->addToArticleArray($rowTemp['article_content'], $rowTemp['where_location']);
						
					}
				}
				
				$this->pageArray[] = $currPage;
			}
			
			return $this->pageArray;
			
		} //end function
		
		
		public function getPageObjects($id) {
			
			//Make Query Modeling
			include_once "Data/queryModel.php";	
			$query = new Query();
			$query->setQueryString($query->queryAllPosts($id));
			$results = $query->queryDatabase();
			
			//Include Customer Entity
			include_once "Page.php";	
						
			while($row = mysqli_fetch_assoc($results)) {
				$currPage = new Page();
				$currPage->setId($row['page_id']);
				$currPage->setPageContent($row['page_content']);
				$currPage->setTempId($row['template_id']);
				$currPage->setBackColor($row['template_backColor']);
				$currPage->setTextColor($row['template_textColor']);
				$currPage->setPageName($row['page_name']);
				$currPage->setWhoMadePage($row['firstname'], $row['lastname']);
				$currPage->setDateMade($row['article_post_date']);
				$currPage->addToArticleArray($row['article_content'], $row['where_location']);
				
				$query->setQueryString($query->queryAllPosts($row['page_id']));
				$resultPosts = $query->queryDatabase();
				$count = mysqli_num_rows($resultPosts);
						
				if($count>0) {
					while($rowTemp = mysqli_fetch_assoc($resultPosts)) {
						$currPage->addToArticleArray($rowTemp['article_content'], $rowTemp['where_location']);
						
					}
				}
				
				$this->pageArray[] = $currPage;
			}
			
			return $this->pageArray;
			
		} //end function
		
		
		public function getSearchPages($searchString) {
			
			//Make Query Modeling
			include_once "Data/queryModel.php";	
			$query = new Query();
			$query->setQueryString($query->search($searchString));
			$results = $query->queryDatabase();
			
			//Include Customer Entity
			include_once "Page.php";	
			
			
			while($row = mysqli_fetch_assoc($results)) {
				$currPage = new Page();
				$currPage->setId($row['page_id']);
				$currPage->setPageContent($row['page_content']);
				$currPage->setTempId($row['template_id']);
				$currPage->setBackColor($row['template_backColor']);
				$currPage->setTextColor($row['template_textColor']);
				$currPage->setPageName($row['page_name']);
				$currPage->setWhoMadePage($row['firstname'], $row['lastname']);
				$currPage->setDateMade($row['article_post_date']);
				$currPage->addToArticleArray($row['article_content'], $row['where_location']);
				
				$query->setQueryString($query->queryAllPosts($row['page_id']));
				$resultPosts = $query->queryDatabase();
				$count = mysqli_num_rows($resultPosts);
						
				if($count>0) {
					while($rowTemp = mysqli_fetch_assoc($resultPosts)) {
						$currPage->addToArticleArray($rowTemp['article_content'], $rowTemp['where_location']);
						
					}
				}
				
				$this->pageArray[] = $currPage;
			}
			
			return $this->pageArray;			
			
		} //end function
		
		
		
		
		
		
	} //end class

?>