$(function() {
	
	addSearchFunctionality();
	
});


function addSearchFunctionality() {
	
	$("#searchGoBtn").click(function() {
	
		var searchInput = $("#searchBox").val();
				
		if(searchInput!="") {
			
			$.post("searchEmployees.php", {criteria: searchInput}, function(data) {
				
				$("#bodyResultArea").html(data);
				$("#searchBox").val(searchInput);
			
			}, "html"); //end post
			
		} else if(searchInput=="") {
			
			$("#bodyResultArea").html("<h2><div class='label label-info>Please enter the name of an employee to search</div></h2>");
			
		} //end if
		
		
	});
	
	
}
	