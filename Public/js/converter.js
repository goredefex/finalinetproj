$(function() {
	
	$("#openFahrenheit").toggle();
	$("#openCelsius").toggle();
	
	
	$("#fahrenheitBtn").click(function() {
		
		$("#openFahrenheit").toggle();
		
	});
	
	
	$("#celsiusBtn").click(function() {
		
		$("#openCelsius").toggle();
		
	});
	
	
});