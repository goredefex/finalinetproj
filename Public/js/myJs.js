var contents;
var validPass = false;


$(function() {
	
	contents = $("#searchZone").html();
	addSearchFunctionality();
	addRegisterFunctionality();
	addPasswordChecker();
	
});



function addSearchFunctionality() {
	
	$("#searchBox").keyup(function() {
		
		var searchText = $("#searchBox").val();
				
		//Size Validation To Stop Heavy Searching
		if(searchText.length>2) {
			
			$.post("searchController.php", {criteria: searchText}, function(data) {
				$("#searchZone").html(data);
				//addNamePopovers();
				
			}, "html"); //end post
						
		} else if(searchText.length==0) {
			$("#searchZone").html(contents);
			
		}
				
	}); //end keyup event
	
} //end function


function addPopover(eleName, personName, empNo) {
	var popBox = "<div>"+personName+"  -  Emp No: "+empNo+"</div>";
	
	$("#"+eleName).popover({
		  html: true,
		  trigger: "hover",
		  container: "#"+eleName,
		  placement: "bottom",
		  content: function () {
			          return popBox;
			  	   } //end function
	
	}); //end popover event
	
} //function end


function addNamePopovers() {
	
	$(".pops").each(function() {
		addPopover($(this).attr("id"), $(this).attr("name"), $(this).attr("id"));
		
	});
	
} //end function



function addPasswordChecker() {
	
	$("#regRePass").keyup(function() {
		
		var pass = $("#regPass").val();
		var rePass = $("#regRePass").val();
		
		if(rePass!="") {
			
			if(rePass.length > 3) {
				
				if(pass===rePass) {
					validPass = true;
					$("#regInput").html("Passwords Match!");
					$("#regInput").attr("style", "border-color: rgb(4, 255, 17)");
					
				} else {
					validPass = false;
					$("#regInput").html("Passwords Do Not Match!");
					$("#regInput").attr("style", "border-color: rgb(250, 20, 10)");
					
				}
				
			} else {
				$("#regInput").html("Password at least 3 char");
				$("#regInput").attr("style", "border-color: rgb(250, 20, 10)");
				
			}
			
		}
		
	});
	
	
} //end function


function addRegisterFunctionality() {
	
	$("#registeration").submit(function(e) {
		
		if(validPass!=true) {
			e.preventDefault();
			
		}
		
	});
	
} //end function









