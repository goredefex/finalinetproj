<?php 

	require "../Controller/pageController.php";
	require "../Controller/loginController.php";
	$page_controls = new PageController();
	$login_control = new LoginController();
	session_start();
	
	
	//Look for user auth level
	if(isset($_POST['user'])) {
		$login_control->processLogin($_POST['user'], $_POST['password']);
		
	} else if(isset($_POST['regUser'])) {
		$login_control->registerUser($_POST['regUser'], $_POST['regPass'], $_POST['regFName'], $_POST['regLName']);
		
	} else if(isset($_GET['logout']) && $_GET['logout']==true) {
		$login_control->processLogout();
		
	} else if(isset($_GET['deleteTemp'])) {
		require_once '../Controller/templateController.php';
		$temp_controls = new TemplateController();
		$temp_controls->deleteTemplate($_GET['deleteTemp']);
		
	} else if(isset($_POST['saveButton'])) {
		require_once '../Controller/templateController.php';
		$temp_controls = new TemplateController();
		$temp_controls->addTemplate($_POST['newTempBackColor'], $_POST['newTempTextColor'], $_POST['newTempDesc']);
		
	} else if(isset($_GET['page'])) {
		if(strcmp($_GET['page'], 'debates')) {
			$_SESSION['debatesPull'] = true;
			$page_controls->buildDebatesPage();
				
		} 
		
	} else if(isset($_GET['pageNum'])) {
		unset($_SESSION['debatesPull']);
		$_SESSION['viewingPosts'] = true;
		$page_controls->generateDebatePage($_GET['pageNum']);
		
	} else if(isset($_GET['tempPage']) && $_GET['tempPage']==true) {
		unset($_SESSION['debatesPull']);
		unset($_SESSION['viewingPosts']);
		require_once '../Controller/templateController.php';
		$temp_controls = new TemplateController();
		$temp_controls->buildTemplateObjects();
		
	} else {
		unset($_SESSION['debatesPull']);
		unset($_SESSION['viewingPosts']);
		$page_controls->buildLandPage();
		
	}
	

?>