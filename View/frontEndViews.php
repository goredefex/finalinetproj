<?php 

	class PageViews {
		
		
		private $articlePlacementArray;
		
						
				
		//Main Displayer of Data & Structure
		public function viewAPage($page, $placements, $articles, $template, $table) {
			
			//Generate Arrays For Easier Zone Guidance
			for($i=0; $i<count($placements); $i++) {
				$articlePlacementArray[$placements[$i]->getLocation()] = $placements[$i]->getArticleId();	
				
			}
						
			ob_start();
			?>
				
				<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	
				<html>
					<head>
						<title>Music B0X - Customer Entries - Halifax Location</title>
						
						<!-- 	### Imported third-Party CSS ###	-->				
						<link href="css/normalize.css" rel="stylesheet">
						<link href="css/bootstrap.css" rel="stylesheet">
					</head>
					
					<header>
						<!-- Fixed Navbar -->
						<div class="navbar navbar-fixed-top navBox" style="max-height: 45px; background-color: rgb(128, 0, 0); color: rgb(255, 255, 255);">
							<div class="container-fluid">
								<a class="brand" href="../index.php">Teh GabB B0x</a>
								<ul class="nav">
									<li><a href="../index.php">Home</a></li>
									<li>
										<?php 
											if(isset($_SESSION['nowLoggedIn']) && $_SESSION['nowLoggedIn']==true) {
												echo "<a href='../index.php?logout=true' id='logoutLink'>Logout</a>";	
												
											} else {
												echo "<a href='#loginBox' id='loginLink' data-toggle='modal'>Login</a>";
												
											}
										?>
									</li>
									<li><a href="#registerBox" data-toggle='modal'>Register</a></li>
									<li id="viewDebate" ><a href="../index.php?page='debates'">Available Debates</a></li>
									<?php 
										if(isset($_SESSION['authority']) && strcmp($_SESSION['authority'], "editor")) {
											echo "<li id='cssTempLink'><a href='../index.php?tempPage=true'>CSS Templates</a></li>";
										}
									?>
								</ul>
								
								<form style="margin-right: 50px; margin-top: 10px;" id="searchBoxWrapper" action="../searchController.php" method="post" class="navbar-search pull-right">  
									<input id="searchBox" name="searcher" type="text" class="search-query" placeholder="Find Author By Name:">
								</form>
								
								
							</div>
								
						</div>
						
						<!-- Hidden Modal -->
						<div id="loginBox" class="modal hide fade" role="dialog" aria-hidden="true">
								  
							<form class="form-inline" name="login" method="post" action="../index.php">				  
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h3 id="modalTitle">Teh GabB B0X - Login</h3>
								</div>
								
								<div id="loginInfo" class="modal-body">
									
									<div id="loginFormBox" class="modalInner">
										<input id="loginName" name="user" type="text" autofocus="true" class="input-medium loginChecker" placeholder="Username">
										<input id="loginPass" name="password" type="password" class="input-medium loginChecker" placeholder="Password">
								    </div>
								   
								</div>
															  
								<div class="modal-footer loginSenders" style="min-height: 100%;">
				    				<div class="pull-left input-xlarge well text-center">
								        <span id="loginInput" class="blackText">
								        	<?php 
								        		if(isset($_SESSION['statusUpdate'])) {
								        			echo $_SESSION['statusUpdate'];	
								        			
								        		} else {
								        			echo "Login Zone!";	
								        			
								        		}
								        	?>
								        	
								        </span>
							  		</div>		    
									<button class="btn btn-large" data-dismiss="modal">Close</button>
									<button type="submit" id="btnLogin" name="loginbtn" class="btn btn-danger saveButton btn-large">Login</button>
								</div>
						    </form>	
						  
						</div>  <!-- End of Modal -->
						
						
						<!-- Hidden Modal -->
						<div id="registerBox" class="modal hide fade" role="dialog" aria-hidden="true">
								  
							<form class="form-inline" id="registeration" name="registeration" method="post" action="../index.php">				  
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h3>Teh GabB B0X - Register</h3>
								</div>
								
								<div class="well text-center">
									
									Username: <input type="text" name="regUser" /> <br />
									Password: <input type="password" name="regPass" id="regPass" /> <br />
									Re-Type Password: <input type="password" name="regRePass" id="regRePass" /> <br />
									First Name: <input type="text" name="regFName" /> <br />
									Last Name: <input type="text" name="regLName" /> <br />
									
								</div>
															  
								<div class="modal-footer" style="min-height: 100%;">
									<div id="regInput" class="pull-left input-xlarge well text-center">
								        <span class="blackText">
								        	Message Zone!					        	
								        </span>
							  		</div>		   
									<button class="btn btn-large" data-dismiss="modal">Close</button>
									<button type="submit" id="regBtn" name="regBtn" class="btn btn-danger saveButton btn-large">Register!</button>
								</div>
						    </form>	
						  
						</div>  <!-- End of Modal -->
					</header>
					
					<body class="container-fluid" style="<?php echo $template->getBackColor(); ?>; <?php echo $template->getTextColor(); ?>;">
						<div id="searchZone" style="text-align: center; margin-top: 50px;">
							<div class="row-fluid label label-important">
								<!-- Zone #1 / Page Content --><?php echo $page->getPageContent(); ?>
							</div>
							
							<?php
								
								if(isset($_SESSION['viewingPosts'])) {
									
									for($i=5; $i<count($page->getArticleArray())+5; $i++) {
										echo "  <!-- Zone 0 - For Top of Page Content -->
												<div class='container-fluid'>
													<div class='hero-unit text-center'>
														".$page->getDateMade()." <hr />
														".$page->getArticleArrayIndex($i)."&nbsp;~
														<span style='color: rgb(255, 255, 255)' class='badge badge-info'>".$page->getWhoMadePage()."</span>
														<hr />
														<button class='label label-inverse'>Rebuttle!</button>
													</div>
												</div>";
										
										if(isset($_SESSION['dbAuth']) && strcmp($_SESSION['authority'], "author")) {
											echo "<button>Author Button</button>";
											
										}		
										
									}
																	
									
								} else {
									
									
									//Static Page Content Section ---------------------------------------
																		
									if(isset($_SESSION['statusUpdate'])) {
										
										echo "  <!-- Session Zone - For Response Content -->
												<div class='container-fluid'>
													<div class='hero-unit text-center'>
														<div class='label-inverse'><h3>".$_SESSION['statusUpdate']."</h3></div>
													</div>
												</div>";
										
									}
							
									if(isset($articles[0])) {
										echo '  <!-- Zone 3 - For Mid Page Content -->
												<div class="container-fluid">
													<div class="hero-unit text-center">
														'.$articles[0]->getContent().'
													</div>
												</div>';
									}
							
									if(isset($articles[2])) {
										echo '  <!-- Zone 3 - For Mid Page Content -->
												<div class="container-fluid">
													<div class="hero-unit text-center">
														'.$articles[2]->getContent().'
													</div>
												</div>';
									}
									
									if(isset($page)) {
										echo '<div class="row-fluid label label-important">
												  <!-- Zone #1 / Page Content -->'.$articles[1]->getContent().'
											  </div>';
										
									}
							
									if(isset($_SESSION['debatesPull']) && isset($table) && $_SESSION['debatesPull'] == true) {
										echo '<!-- Zone Posts - For Mid Page Content -->
												<div class="container-fluid text-center">
													<div class="hero-unit">
														'.$this->viewTopicTable($table).'
													</div>
												</div>';
									}
								}
							?>	
							
							
						</div>
						
					<!--    $$$	Imported Third-party Scripts $$$	-->
					<script type="text/javascript" src="js/jquery-2.0.2.js"></script>
					<script type="text/javascript" src="js/modernizr-2.6.2.min.js"></script>
					<script type="text/javascript" src="js/bootstrap.js"></script>
					<script type="text/javascript" src="js/myJs.js"></script>
						
					</body>
					
					
					
				</html>
					
			<?php
			$output = ob_get_clean();
			ob_flush();
			
						
			return $output;
						
		} //end function
		
		
		
		//Function To Generate A Table
		public function viewTopicTable($tableData) {
			
			ob_start();
			?>
				<table border="1">
					<thead>
						<tr>
							<th>Page Name</th>
							<th>Created By</th>							
						</tr>
					</thead>					
					<tbody>
						<?php 
							for($i=1; $i<count($tableData); $i++) {
		
								echo "<tr><td>".$tableData[$i]->getPageName()."</td><td>".$tableData[$i]->getWhoMadePage()."</td></tr>";
								
							}
						?>
					</tbody>		
				</table>	
				
			<?php
			$output = ob_get_clean();
			ob_flush();
			
			return $output;
			
		} //end function 
		
		
		//Function To Generate A Table
		public function viewSearchTable($tableData) {
			
			ob_start();
			?>
				<table border="1">
					<thead>
						<tr>
							<th>Author Id</th>
							<th>Created By</th>
							<th>Date Made</th>
							<th>Debate Title</th>
						</tr>
					</thead>					
					<tbody>
						<?php 
							for($i=0; $i<count($tableData); $i++) {
		
								echo "<tr><td>".$tableData[$i]->getId()."</td><td>".$tableData[$i]->getWhoMadePage()."</td>
									  <td>".$tableData[$i]->getDateMade()."</td><td><a href='../index.php?pageNum=".$tableData[$i]->getId()."'>
									  		".$tableData[$i]->getPageName()."</a></td></tr>";
								
							}
						?>
					</tbody>		
				</table>	
				
			<?php
			$output = ob_get_clean();
			ob_flush();
			
			return $output;
			
		} //end function 
		
		
		
		public function viewDebatePage($pageObj) {
					
			ob_start();
			?>	
			
				<form class="form-inline" action="../index.php" method="post">
			
					<div class="container" style="max-width: 400px; margin-top: 50px;">
						
						<div class="controls-row">
					        <span class="help-inline control-list">Customer Id:</span>
					        <div class="input-prepend pull-right">
					        	<label class="label label-info add-on blackText">*</label>
					        	<input style="min-height: 30px;" name="id" type="text" class="input-medium" value="<?php echo $customer->getId(); ?>" />
					        </div>
					    </div>
						
						 <div class="controls-row">
					        <span class="help-inline control-list">First Name:</span>
					        <div class="input-prepend pull-right">
					        	<label class="label label-info add-on blackText">*</label>
					        	<input style="min-height: 30px;" name="fName" type="text" class="input-medium" value="<?php echo $customer->getFName(); ?>" />
					        </div>
					    </div>
					    
					    <div class="controls-row">
					        <span class="help-inline control-list">Last Name:</span>
					        <div class="input-prepend pull-right">
					        	<label class="label label-info add-on blackText">*</label>
					        	<input style="min-height: 30px;" name="lName" type="text" class="input-medium" value="<?php echo $customer->getLName(); ?>" />
					        </div>
					    </div>
					    
					     <div class="controls-row">
					        <span class="help-inline control-list">Email:</span>
					        <div class="input-prepend pull-right">
					        	<label class="label label-info add-on blackText">*</label>
					        	<input style="min-height: 30px; min-width: 350px;" name="email" type="text" class="input-large" value="<?php echo $customer->getEmail(); ?>" />
					        </div>
					    </div>
					    	    			    		    
				    	<button name="updateBtn" class="btn btn-success">Submit</button>
				    
				    </div>
			    
			    </form>
			
			<?php
			$output = ob_get_clean();
			ob_flush();
			
			return $output;
			
		} //end function
		
		
		public function viewTempPage($temps) {
					
			ob_start();
			?>	
			<html>
				<head>
					<title>Music B0X - Customer Entries - Halifax Location</title>
					
					<!-- 	### Imported third-Party CSS ###	-->				
					<link href="css/normalize.css" rel="stylesheet">
					<link href="css/bootstrap.css" rel="stylesheet">
				</head>
				<header>
					<!-- Fixed Navbar -->
					<div class="navbar navbar-fixed-top navBox" style="max-height: 45px; <?php echo $temps[0]->getBackColor() ?>" color: "<?php echo $temps[0]->getTextColor() ?>">
						<div class="container-fluid">
							<a class="brand" href="../index.php">Teh GabB B0x</a>
							<ul class="nav">
								<li><a href="../index.php">Home</a></li>
								<li>
									<?php 
										if(isset($_SESSION['nowLoggedIn']) && $_SESSION['nowLoggedIn']==true) {
											echo "<a href='../index.php?logout=true' id='logoutLink'>Logout</a>";	
											
										} else {
											echo "<a href='#loginBox' id='loginLink' data-toggle='modal'>Login</a>";
											
										}
									?>
								</li>
								<li><a href="#registerBox" data-toggle='modal'>Register</a></li>
								<li id="viewDebate" ><a href="../index.php?page='debates'">Available Debates</a></li>
								<?php 
									if(isset($_SESSION['authority']) && strcmp($_SESSION['authority'], "editor")) {
										echo "<li id='cssTempLink'><a href='../index.php?tempPage=true'>CSS Templates</a></li>";
									}
								?>
							</ul>
							
							<form style="margin-right: 50px; margin-top: 10px;" id="searchBoxWrapper" action="../searchController.php" method="post" class="navbar-search pull-right">  
								<input id="searchBox" name="searcher" type="text" class="search-query" placeholder="Find Author By Name:">
							</form>
							
							
						</div>
							
					</div>
					
					<!-- Hidden Modal -->
					<div id="loginBox" class="modal hide fade" role="dialog" aria-hidden="true">
							  
						<form class="form-inline" name="login" method="post" action="../index.php">				  
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h3 id="modalTitle">Teh GabB B0X - Login</h3>
							</div>
							
							<div id="loginInfo" class="modal-body">
								
								<div id="loginFormBox" class="modalInner">
									<input id="loginName" name="user" type="text" autofocus="true" class="input-medium loginChecker" placeholder="Username">
									<input id="loginPass" name="password" type="password" class="input-medium loginChecker" placeholder="Password">
							    </div>
							   
							</div>
														  
							<div class="modal-footer loginSenders" style="min-height: 100%;">
			    				<div class="pull-left input-xlarge well text-center">
							        <span id="loginInput" class="blackText">
							        	<?php 
							        		if(isset($_SESSION['statusUpdate'])) {
							        			echo $_SESSION['statusUpdate'];	
							        			
							        		} else {
							        			echo "Login Zone!";	
							        			
							        		}
							        	?>
							        	
							        </span>
						  		</div>		    
								<button class="btn btn-large" data-dismiss="modal">Close</button>
								<button type="submit" id="btnLogin" name="loginbtn" class="btn btn-danger saveButton btn-large">Login</button>
							</div>
					    </form>	
					  
					</div>  <!-- End of Modal -->
					
					
					<!-- Hidden Modal -->
					<div id="registerBox" class="modal hide fade" role="dialog" aria-hidden="true">
							  
						<form class="form-inline" id="registeration" name="registeration" method="post" action="../index.php">				  
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h3>Teh GabB B0X - Register</h3>
							</div>
							
							<div class="well text-center">
								
								Username: <input type="text" name="regUser" /> <br />
								Password: <input type="password" name="regPass" id="regPass" /> <br />
								Re-Type Password: <input type="password" name="regRePass" id="regRePass" /> <br />
								First Name: <input type="text" name="regFName" /> <br />
								Last Name: <input type="text" name="regLName" /> <br />
								
							</div>
														  
							<div class="modal-footer" style="min-height: 100%;">
								<div id="regInput" class="pull-left input-xlarge well text-center">
							        <span class="blackText">
							        	Message Zone!					        	
							        </span>
						  		</div>		   
								<button class="btn btn-large" data-dismiss="modal">Close</button>
								<button type="submit" id="regBtn" name="regBtn" class="btn btn-danger saveButton btn-large">Register!</button>
							</div>
					    </form>	
					  
					</div>  <!-- End of Modal -->
				</header>
				<body style="padding-top: 80px;" class="row-fluid">
					<form class="form-inline" action="../index.php" method="post">
				
						<?php 
							
							for($i=0; $i<count($temps); $i++) {
								?>
								<hr />
								<div class="row-fluid span12" style="margin-bottom: 80px;">
									<div class="span2">
									    <span class="help-inline control-list">Temp Id:</span>
									    <div class="input-prepend pull-right">
										    <label class="label label-info add-on blackText">*</label>
										    <input style="min-height: 30px;" name="tempId" type="text" class="input-small" value="<?php echo $temps[$i]->getTempId(); ?>" />
									    </div>
								    </div>		
								    <div class="span3">
									    <span class="help-inline control-list">Back Color:</span>
									    <div class="input-prepend pull-right">
										    <label class="label label-info add-on blackText">*</label>
										    <input style="min-height: 30px;" name="tempBackColor" type="text" class="input-large" value="<?php echo $temps[$i]->getBackColor(); ?>" />
									    </div>
								    </div>		
								    <div class="span3">
									    <span class="help-inline control-list">Text Color:</span>
									    <div class="input-prepend pull-right">
										    <label class="label label-info add-on blackText">*</label>
										    <input style="min-height: 30px;" name="tempTextColor" type="text" class="input-medium" value="<?php echo $temps[$i]->getTextColor(); ?>" />
									    </div>
								    </div>			
								    <div class="span3">
									    <span class="help-inline control-list">Desc:</span>
										<input style="min-height: 30px;" name="tempDesc" type="text" class="input-medium" value="<?php echo $temps[$i]->getDescription(); ?>" />
								    </div>
								    <button id="deleteButton"><a href="../index.php?deleteTemp=<?php echo $temps[$i]->getTempId(); ?>">Delete!</a></button>					    
								</div>
								<?php
							}
						
						?>
							
					   <button type="button" class="btn btn-success" onclick="$('#newTempBox').toggle()">Add New Template!</button>
					   
					   <!-- Hidden Unit -->
					   <div id="newTempBox" style="display: none;" class="hero-unit">
					   	
						   	<div class="row-fluid span12" style="margin-bottom: 80px;">
						   		<div class="span3">
							   		<span class="help-inline control-list">New Background Color:</span>
									<select style="min-height: 30px;" name="newTempBackColor" type="text" class="input-small">
										<option value="background-color: rgb(255, 0, 0)">Red</option>								
										<option value="background-color: rgb(0, 0, 0)">Black</option>
										<option value="background-color: rgb(255, 255, 255)">White</option>
										<option value="background-color: rgb(34, 177, 76)">Green</option>
										<option value="background-color: rgb(0, 0, 255)">Blue</option>
									</select>
								</div>
								<div class="span3">
							   		<span class="help-inline control-list">New Text Color:</span>
									<select style="min-height: 30px;" name="newTempTextColor" type="text" class="input-small">
										<option value="background-color: rgb(255, 0, 0)">Red</option>								
										<option value="background-color: rgb(0, 0, 0)">Black</option>
										<option value="background-color: rgb(255, 255, 255)">White</option>
										<option value="background-color: rgb(34, 177, 76)">Green</option>
										<option value="background-color: rgb(0, 0, 255)">Blue</option>
									</select>
								</div>
								<div class="span4">
							   		<span class="help-inline control-list">New Description:</span>
									<input style="min-height: 30px;" name="newTempDesc" type="text" class="input-medium" />
								</div>
								<div class="span2">
									<button type="submit" name="saveButton" class="btn btn-danger">Save!</button>
								</div>
							</div>
					   </div>
				    </form>
				    
				    <!--    $$$	Imported Third-party Scripts $$$	-->
					<script type="text/javascript" src="js/jquery-2.0.2.js"></script>
					<script type="text/javascript" src="js/modernizr-2.6.2.min.js"></script>
					<script type="text/javascript" src="js/bootstrap.js"></script>
					<script type="text/javascript" src="js/myJs.js"></script>
					
				</body>
				
				</html>
				
			<?php
			$output = ob_get_clean();
			ob_flush();
			
			return $output;
			
		} //end function
		
		
		
		
		
	} //end class

?>